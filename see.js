var OPERATION = {
	'==':                      1,
	'!=':                      2,
	'!':                       3,
	'&&':                      4,
	'||':                      5,
	'>':                       6,
	'>=':                      7,
	'<':                       8,
	'<=':                      9,

	'-':                      10,
	'+':                      11,
	'/':                      12,
	'*':                      13,

	'string':                 14,
	'number':                 15,
	'boolean':                16,

	'functional':             17,
	'parameter':              18,
	'parenthetical':          19,

	'variable':               20,
	'component':              21,

	'compiled':               22
};

var OPERATION_LABEL = {
	1: 'equals',
	2: 'not equal to',
	3: 'not',
	4: 'and',
	5: 'or',
	6: 'greater than',
	7: 'greater than or equal to',
	8: 'lesser than',
	9: 'lesser than or equal to',

	10: 'minus',
	11: 'plus',
	12: 'divided by',
	13: 'times',

	14: 'string',
	15: 'number',
	16: 'boolean',

	17: 'function',
	18: 'parameter',
	19: 'parenthetical',

	20: 'variable',
	21: 'component property',

	22: 'compiled'
};

var OPERATION_FUNCTION = {
	// custom methods
	BETWEEN: function (a, b, c) { return a >= b && a <= c; },
	BETWEENR: function (a, b, c) { return OPERATION_FUNCTION.BETWEEN(a, b - (c / 2), b + (c / 2)); },

	// casting methods
	INTEGER: function (n) { return parseInt(n, 10); },
	REAL: function (n) { return parseFloat(n, 10); },
	FLOAT: function (n) { return OPERATION_FUNCTION.REAL(n); },
	STRING: function (s) { return String(s); },

	// ES5 Math functions
	// http://www.ecma-international.org/ecma-262/5.1/#sec-15.8
	ABS: Math.abs,
	ACOS: Math.acos,
	ASIN: Math.asin,
	ATAN: Math.atan,
	ATAN2: Math.atan2,
	CEIL: Math.ceil,
	COS: Math.cos,
	EXP: Math.exp,
	FLOOR: Math.floor,
	LOG: Math.log,
	MAX: Math.max,
	MIN: Math.min,
	POW: Math.pow,
	RANDOM: Math.random,
	ROUND: Math.round,
	SIN: Math.sin,
	SQRT: Math.sqrt,
	TAN: Math.tan
};

var OPERATION_ACTION = {
	 1: function (a, b) { /*jshint eqeqeq:false */ return a == b; },
	 2: function (a, b) { /*jshint eqeqeq:false */ return a != b; },
	 3: function (a) { return !a; },
	 4: function (a, b) { return (!a && !b) ? a : b; },
	 5: function (a, b) { return a || b; },
	 6: function (a, b) { return a >  b; },
	 7: function (a, b) { return a >= b; },
	 8: function (a, b) { return a <  b; },
	 9: function (a, b) { return a <= b; },

	10: function (a, b) { return a - b; },
	11: function (a, b) { return a + b; },
	12: function (a, b) { return a / b; },
	13: function (a, b) { return a * b; }
};

/*******************************************************************************
* CONSTRUCTOR
*******************************************************************************/

function SEE(formula, callback) {
	this.formula = String(formula);
	this.callback = (typeof callback === 'function') ? callback : function () { };

	this.parseFormula();
	this.executeFormula();
}

SEE.prototype = {
	TYPE_VARIABLE: 1,
	TYPE_COMPONENT: 2,

	VARIABLE_NOTFOUND: -1,
	COMPONENT_NOTFOUND: -1,
	PROPERTY_NOTFOUND: -2,

/*******************************************************************************
* CLASS VARIABLES
*******************************************************************************/

	callback: undefined,

	formula: undefined,
	convertedFormula: undefined,
	_value: undefined,
	_originalValue: undefined,

/*******************************************************************************
* METHODS
*******************************************************************************/

	value: function () {
		return this._value;
	},

	originalValue: function () {
		return this._originalValue;
	},

	parseFormula: function () {
		var formula = this.formula;

		var equation = [];
		var context = equation;

		var previousCharacterWasSpace = false;
		var mode = 'none';
		var current = '';
		var character;

		var errorMessage = function (message, index) {
			var errorMessage = 'SEE: ' + message;

			if (index !== undefined) {
				errorMessage += ' at character ' + (~~index + 1);
			}

			errorMessage += '\n' + formula + '\n';

			if (index !== undefined) {
				for (var i=0; i<index; i++) {
					errorMessage += '-';
				}

				errorMessage += '^';
			}

			return errorMessage;
		};

		var addVariable = function (currentIndex) {
			if (!current.length) { return; }

			var operation, property, trueIndex;
			var type = current.charAt(0);
			var name = current.substr(1);

			switch (type) {
			case '@':
				operation = OPERATION.component;

				var pieces = name.split('.');
				if (pieces.length > 2) {
					trueIndex = formula.indexOf(current);
					trueIndex = current.indexOf('.', current.indexOf('.') + 1);
					throw new Error(errorMessage('unexpected .', trueIndex));
				}

				name = pieces[0];
				property = pieces[1];
				break;
			case '$':
				if (name.indexOf('.') !== -1) {
					trueIndex = formula.indexOf(current);
					trueIndex += current.indexOf('.');
					throw new Error(errorMessage('unexpected .', trueIndex));
				}

				operation = OPERATION.variable;
				break;

			default:
				var value = current;
				if (this.isInteger(+value) || this.isFloat(+value)) {
					operation = OPERATION.number;
					value = +value;
				} else {
					operation = OPERATION.boolean;
					switch (value) {
					case 'true':
						value = true;
						break;

					case 'false':
						value = false;
						break;

					default:
						trueIndex = formula.indexOf(current);
						throw new Error(errorMessage('unexpected value', trueIndex));
					}
				}

				current = '';

				return context.push({
					operation: operation,
					label: OPERATION_LABEL[operation],
					value: value
				});
			}

			context.push({
				operation: operation,
				label: OPERATION_LABEL[operation],
				name: name,
				property: property
			});

			current = '';
		}.bind(this);

		var addString = function () {
			var operation = OPERATION.string;

			context.push({
				operation: operation,
				label: OPERATION_LABEL[operation],
				value: current
			});
			current = '';
		};

		var addOperation = function (operation) {
			context.push({
				operation: operation,
				label: OPERATION_LABEL[operation]
			});
		};

		var i = 0;
		for (; i<formula.length; i++) {
			character = formula.charAt(i);

			// deal with character
			var operator;
			switch (mode) {
			case 'none':
				previousCharacterWasSpace = false;

				switch (character) {
				case ' ':
					// spaces don't cause syntax errors at this mode
					continue;

				case '=':
				case '&':
				case '|':
				case '>':
				case '<':
				case ';':
				case '`':
				case '{':
				case '}':
				case '[':
				case ']':
				case '?':
				case '~':
				case '.':
				case '#':
				case '%':
				case '^':
				case '_':
				case ':':
					throw new Error(errorMessage('unexpected ' + character, i));

				default:
					i -= 1;
					mode = 'value';
					break;
				}
				break;

			case 'value':
				switch (character) {
				case '!':
				case '>':
				case '<':
					addVariable(i);

					operator = character;
					if (formula.charAt(i + 1) === '=') {
						operator = operator + '=';
						i += 1;
					}

					addOperation(OPERATION[operator]);

					mode = 'operator';
					previousCharacterWasSpace = false;
					break;

				case '=':
				case '&':
				case '|':
					i += 1;

					addVariable(i);

					if (formula.charAt(i) !== character) {
						throw new Error(errorMessage('expected ' + character, i));
					}

					operator = character + character;
					addOperation(OPERATION[operator]);

					mode = 'operator';
					previousCharacterWasSpace = false;
					break;

				case '+':
				case '-':
					addVariable(i);
					addOperation(OPERATION[character]);
					mode = 'operator-add/sub';
					previousCharacterWasSpace = false;
					break;

				case '/':
				case '*':
					addVariable(i);
					addOperation(OPERATION[character]);
					mode = 'operator';
					previousCharacterWasSpace = false;
					break;

				case '(':
					var newContext = [];
					newContext.parent = context;

					operator = 'parenthetical';

					// if current exists, it's the function name
					// we'll find out later if it actually exists
					if (current.length) {
						operator = 'functional';

						newContext.functionName = current;
						current = '';
					}

					newContext.operation = OPERATION[operator];
					newContext.label = OPERATION_LABEL[newContext.operation];

					context = newContext;
					context.parent.push(context);

					mode = 'operator';
					previousCharacterWasSpace = false;
					break;

				case ')':
					addVariable(i);

					mode = 'operator-)';

					context = context.parent;
					if (context === undefined) {
						throw new Error(errorMessage('unexpected ' + character, i));
					}

					mode = 'operator-)';
					previousCharacterWasSpace = false;
					break;

				case '\'':
				case '"':
					if (current.length) {
						throw new Error(errorMessage('unexpected ' + character, i));
					}

					mode = 'string';
					previousCharacterWasSpace = false;
					break;

				case ',':
					if (!context.functionName) {
						throw new Error(errorMessage('unexpected ' + character, i));
					}

					addVariable(i);
					addOperation(OPERATION.parameter);

					mode = 'none';
					previousCharacterWasSpace = false;
					break;

				case ' ':
					previousCharacterWasSpace = true;
					break;

				case ';':
				case '`':
				case '{':
				case '}':
				case '[':
				case ']':
				case '\\':
				case '?':
				case '~':
				case '#':
				case '%':
				case '^':
				case ':':
					throw new Error(errorMessage('unexpected ' + character, i));

				default:
					if (previousCharacterWasSpace) {
						throw new Error(errorMessage('unexpected \' \'', i - 1));
					}

					current += character;
					previousCharacterWasSpace = false;
					break;
				}
				break;

			case 'operator':
				switch (character) {
				case '=':
				case '&':
				case '|':
				case '>':
				case '<':
				case '+':
				case '-':
				case '/':
				case '*':
				case ',':
				case ';':
				case '`':
				case '{':
				case '}':
				case '[':
				case ']':
				case '\\':
				case '?':
				case '.':
				case '~':
				case '#':
				case '%':
				case '^':
				case '_':
				case ':':
					throw new Error(errorMessage('unexpected ' + character, i));

				case ')':
					if (!context.functionName) {
						throw new Error(errorMessage('unexpected ' + character, i));
					}

					mode = 'value';
					i -= 1;
					break;

				case '!':
					mode = 'value';
					i -= 1;
					break;

				default:
				case '\'':
				case '"':
					mode = 'none';
					i -= 1;
					break;
				}
				break;

			case 'operator-add/sub':
				switch (character) {
				case '=':
				case '&':
				case '|':
				case ')':
				case '>':
				case '<':
				case '/':
				case '*':
				case ';':
				case '{':
				case '}':
				case '[':
				case ']':
				case '\\':
				case '?':
				case '.':
				case '~':
				case '#':
				case '%':
				case '^':
				case '_':
				case ':':
					throw new Error(errorMessage('unexpected ' + character, i));

				case '+':
				case '-':
					mode = 'value';
					i -= 1;
					break;

				default:
					mode = 'none';
					i -= 1;
					break;
				}
				break;

			case 'operator-)':
				switch (character) {
				case '>':
				case '<':
				case '/':
				case '*':
				case '=':
				case '&':
				case '|':
				case '+':
				case '-':
				case ')':
				case ' ':
				case ',':
					mode = 'value';
					i -= 1;
					break;

				default:
				case ';':
				case '`':
				case '{':
				case '}':
				case '[':
				case ']':
				case '\\':
				case '?':
				case '.':
				case '~':
				case '#':
				case '%':
				case '^':
				case '_':
				case ':':
					throw new Error(errorMessage('unexpected ' + character, i));
				}
				break;

			case 'string':
				switch (character) {
				case '\'':
				case '"':
					addString();
					mode = 'post-string';
					break;

				default:
					current += character;
					break;
				}
				break;

			case 'post-string':
				switch (character) {
				case ' ':
					continue;

				case '=':
				case '&':
				case '|':
				case '>':
				case '<':
				case '+':
				case '-':
				case '/':
				case '*':
				case '!':
				case ')':
					mode = 'value';
					i -= 1;
					break;

				default:
					throw new Error(errorMessage('unexpected ' + character, i));
				}
			} // switch
		} // for

		// clean up
		if (current.length) {
			switch (mode) {
			case 'value':
				addVariable(i);
				mode = 'none';
				break;
			case 'post-string':
			case 'operator-)':
				mode = 'none';
			}
		}

		// we should end in mode 'none' or something similar
		switch (mode) {
		case 'operator':
			throw new Error(errorMessage('unexpected ' + character + ', got EOF', i));

		case 'string':
			throw new Error(errorMessage('expected \', got EOF', i));

		case 'post-string':
		case 'operator-)':
		case 'none':
			break;

		default:
			throw new Error(errorMessage('expected EOF', i));
		}

		// and if we're not at the original context, there's a missing )
		if (context !== equation) {
			throw new Error(errorMessage('expected ), got EOF', i));
		}

		//
		this.convertedFormula = equation;
	},

	executeFormula: function () {
		// PEMDAS --> PEMA --> PF(*)MA
		// 1) Exponents are a (F)unction (POW)
		// 2) Function need its parameters calculated before execution
		// 3) Function parameter calculation is also PF(*)MA

		// the entire equation is a parenthetical itself
		//console.time('compilation took:');
		var originalValue = this.compileParenthetical(this.convertedFormula);
		//console.timeEnd('compilation took:');

		this._originalValue = originalValue.value;

		// convert the final value to boolean
		this._value = !!originalValue.value;
	},

	compileParenthetical: function (parenthetical) {
		parenthetical = parenthetical || [];

		// if empty, set parenthetical to false
		if (!parenthetical.length) {
			return parenthetical.push({
				operation: OPERATION.compiled,
				label: OPERATION_LABEL[OPERATION.compiled],
				value: false
			});
		}

		// calculate values for parentheticals, functionals & externals
		var callback = this.callback;
		var compileParenthetical = this.compileParenthetical.bind(this);
		var compileFunctional = this.compileFunctional.bind(this);
		parenthetical.forEach(function (operation, i) {
			switch (operation.operation) {
			case OPERATION.parenthetical:
				parenthetical[i] = compileParenthetical(operation);
				break;
			case OPERATION.functional:
				parenthetical[i] = compileFunctional(operation);
				break;
			case OPERATION.variable:
				try {
					operation.value = callback(this, this.TYPE_VARIABLE, operation.name);
				} catch (e) {
					throw new Error('SEE: variable ' + operation.name + ' is undefined');
				}
				break;
			case OPERATION.component:
				try {
					operation.value = callback(this, this.TYPE_COMPONENT, operation.name, operation.property);
				} catch (e) {
					switch (e) {
					case this.COMPONENT_NOTFOUND:
						throw new Error('SEE: component ' + operation.name + ' is undefined');

					case this.PROPERTY_NOTFOUND:
						throw new Error('SEE: component property ' + operation.property + ' undefined');
					}
				}
				break;
			}
		}, this);

		//
		this.processNegationOperations(parenthetical);

		// compile multiplication/division operation
		this.compileOperations(parenthetical, [
			OPERATION['*'],
			OPERATION['/']
		]);

		// compile addition/subtraction operations
		this.compileAdditionAndSubtractionOperation(parenthetical);

		// compile equality operations
		this.compileOperations(parenthetical, [
			OPERATION['=='],
			OPERATION['!='],
			OPERATION['>'],
			OPERATION['>='],
			OPERATION['<'],
			OPERATION['<=']
		]);

		// perform AND/OR tests
		this.compileLogicalOperations(parenthetical);

		//
		if (parenthetical.length > 1) {
			throw new Error('SEE: formula could not be compiled');
		}

		return parenthetical[0];
	},

	compileFunctional: function (parenthetical) {
		var functionName = String(parenthetical.functionName).toUpperCase();
		var functional = OPERATION_FUNCTION[functionName];

		if (!functional) {
			throw new Error('SEE: ' + parenthetical.functionName + ' is not defined');
		}

		// generate parentheticals from parameters
		var comma = OPERATION.parameter;

		var parameters = [];
		var parameter = [];

		parenthetical.forEach(function (operation) {
			if (operation.operation === comma) {
				parameters.push(parameter);
				parameter = [];
				return;
			}
			parameter.push(operation);
		});

		parameters.push(parameter);

		// execute generated parentheticals
		parameters = parameters.map(function (parameterParenthetical) {
			return (this.compileParenthetical(parameterParenthetical) || {}).value;
		}, this);

		// now run function
		var value = functional.apply(undefined, parameters);

		// return value
		var operationType = OPERATION.compiled;
		return {
			operation: operationType,
			label: OPERATION_LABEL[operationType],
			value: value
		};
	},

	// negation flipped the boolean value of the following operation
	processNegationOperations: function (parenthetical) {
		var negation = OPERATION['!'];

		var operationExists = function () {
			return parenthetical.some(function (operation) {
				return (operation.operation === negation);
			});
		};

		var negationAction = OPERATION_ACTION[negation];
		var processOperation = function (operation, i) {
			if (operation.operation !== negation) { return false; }

			// if next operation is also negation
			// add one to the negation operations to apply to a value operation
			var nextOperation = parenthetical[i + 1];
			if (nextOperation.operation === negation) {
				nextOperation.additionalNegation = ~~operation.additionalNegation + 1;
				parenthetical.splice(i, 1);
				return true;
			}

			var value = nextOperation.value;

			var jc = 1 + ~~operation.additionalNegation;
			for (var j = 0; j < jc; j++) {
				value = negationAction(value);
			}

			var type = OPERATION.compiled;
			parenthetical.splice(i, 2, {
				type: type,
				label: OPERATION_LABEL[type],
				value: value
			});

			return true;
		};

		while (operationExists()) {
			parenthetical.some(processOperation);
		}
	},

	compileAdditionAndSubtractionOperation: function (parenthetical) {
		var addition = OPERATION['+'];
		var subtraction = OPERATION['-'];
		var operations = [addition, subtraction];

		var operationExists = function () {
			return parenthetical.some(function (operation) {
				return (operations.indexOf(operation.operation) >= 0);
			});
		};

		var processOperation = function (operation, i) {
			var operationType = operation.operation;
			if (operations.indexOf(operationType) < 0) { return false; }

			var nextOperation = parenthetical[i + 1];
			switch (operationType) {
			case addition:
				switch (nextOperation.operation) {
				// + + = +
				case addition:
					parenthetical.splice(i, 1);
					return true;

				// + - = -
				case subtraction:
					parenthetical.splice(i, 1);
					return true;
				}
				break;

			case subtraction:
				switch (nextOperation.operation) {
				// - + = -
				case addition:
					parenthetical.splice(i + 1, 1);
					return true;

				// - - = +
				case subtraction:
					parenthetical.splice(i, 2, {
						operation: addition,
						label: OPERATION_LABEL[addition]
					});
					return true;
				}
				break;
			}

			//
			var operation1 = parenthetical[i - 1];
			var operation2 = parenthetical[i + 1];

			// there are only two operations total
			if (!operation1) {
				switch (operationType) {
				case addition:
					parenthetical.splice(i, 2, operation2);
					return true;

				case subtraction:
					operation2.value = -operation2.value;
					parenthetical.splice(i, 2, operation2);
					return true;
				}

			}

			var action = OPERATION_ACTION[operationType];
			var value = action(operation1.value, operation2.value);

			operationType = OPERATION.compiled;
			parenthetical.splice(i - 1, 3, {
				type: operationType,
				label: OPERATION_LABEL[operationType],
				value: value
			});
		};

		while (operationExists()) {
			parenthetical.some(processOperation);
		}
	},

	compileOperations: function (parenthetical, operations) {
		var operationExists = function () {
			return parenthetical.some(function (operation) {
				return (operations.indexOf(operation.operation) >= 0);
			});
		};

		var processOperation = function (operation, i) {
			var operationType = operation.operation;
			if (operations.indexOf(operationType) < 0) { return false; }

			//
			var operation1 = parenthetical[i - 1];
			var operation2 = parenthetical[i + 1];

			var action = OPERATION_ACTION[operationType];
			var value = action(operation1.value, operation2.value);

			operationType = OPERATION.compiled;
			parenthetical.splice(i - 1, 3, {
				type: operationType,
				label: OPERATION_LABEL[operationType],
				value: value
			});
		};

		while (operationExists()) {
			parenthetical.some(processOperation);
		}
	},

	compileLogicalOperations: function (parenthetical) {
		var logicalAnd = OPERATION['&&'];
		var logicalOr = OPERATION['||'];
		var operations = [logicalAnd, logicalOr];

		// skip processing entirely if no &&/|| operations exists
		if (!parenthetical.some(function (operation) {
			return parenthetical.some(function (operation) {
				return (operations.indexOf(operation.operation) >= 0);
			});
		})) { return; }

		// process non-&&/|| operations
		var lastOperationWasTrue = false;
		parenthetical.some(function (operation, i) {
			var operationType = operation.operation;

			switch (operationType) {
			case logicalOr:
				return lastOperationWasTrue;

			case logicalAnd:
				return !lastOperationWasTrue;

			default:
				lastOperationWasTrue = operation.value;
				return false;
			}
		});

		while (parenthetical.length) {
			parenthetical.pop();
		}

		var operationType = OPERATION.compiled;
		parenthetical.push({
			operation: operationType,
			label: OPERATION_LABEL[operationType],
			value: lastOperationWasTrue
		});
	},

/*******************************************************************************
* HELPERS
*******************************************************************************/

	isInteger: function (n) {
		return n === +n && n !== (n|0);
	},

	isFloat: function (n) {
		return n === +n && n === (n|0);
	}

/*******************************************************************************
* STATIC METHODS
*******************************************************************************/
};

SEE.execute = function (formula, callback) {
	var parser = new SEE(formula, callback);
	return parser;
};

SEE.TYPE_VARIABLE = 1;
SEE.TYPE_COMPONENT = 2;

SEE.VARIABLE_NOTFOUND = -1;
SEE.COMPONENT_NOTFOUND = -1;
SEE.PROPERTY_NOTFOUND = -2;

/*******************************************************************************
* /END
*******************************************************************************/

module.exports = SEE;
