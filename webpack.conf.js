var _ = require('lodash');
var path = require('path');
var webpack = require('webpack');

var config = {
	module: {
		loaders: [{
			test: /\.es6$/,
			exclude: /node\_modules/i,
			loaders: ['babel?nonStandard=true']
		}]
	},
	progress: true,
	resolve: {
		extensions: ['.es6', '.js', ''],
		root: __dirname
	},
	stats: {
		modules: true,
		reasons: true
	},
	target: 'web'
};

module.exports = {
	development: function () {
		return _.extend({}, config, {
			cache: true,
			devtool: '#source-map',
			entry: {
				app: ['./main.es6']
			},
			output: {
				path: './',
				filename: 'bundle.js'
			},
			plugins: [
				new webpack.DefinePlugin({
					__DEVELOPMENT__: true
				})
			],
			sourceMapFilename: '[file].map'
		});
	}
};
