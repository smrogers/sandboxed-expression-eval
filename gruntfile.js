/*globals module*/
/*jshint node:true, camelcase:false*/

'use strict';

var webpackConfig = require('./webpack.conf.js');

module.exports = function (grunt) {
	require('jit-grunt')(grunt, {
		'webpack-dev-server': 'grunt-webpack',
		'eslint': 'gruntify-eslint'
	});

	var pkg = grunt.file.readJSON('package.json');

	grunt.initConfig({
		pkg: pkg,

		bump: {
			options: {
				files: ['package.json', 'bower.json'],
				commit: false,
				createTag: false,
				push: false,
				force: true
			}
		},

		'webpack-dev-server': {
			options: {
				webpack: webpackConfig.development(),
				contentBase: './',
				inline: true
			},
			start: {
				webpack: {
					debug: true,
					inline: true
				},
				keepAlive: true,
				host: 'localhost',
				port: 3000
			}
		},

		eslint: {
			options: {
				config: './.eslintrc'
			},

			all: {
				src: [
					'./see.js',
					'./*.es6'
				]
			}
		},

		githooks: {
			all: {
				'pre-commit': 'eslint:all karma:start'
			}
		}
	});

/*******************************************************************************
* TASKS
*******************************************************************************/

	grunt.registerTask('default', [
		'webpack-dev-server:start'
	]);

	grunt.registerTask('lint', [
		'eslint'
	]);
};
