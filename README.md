# Sandboxed Expression Eval

A sandboxed version of Javascript's eval for evaluating expressions for truthiness: 1 + 1 == 2, etc. Variables are piped to callbacks.